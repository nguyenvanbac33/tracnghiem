package com.nor.common;

import org.apache.commons.fileupload.FileItem;

import javax.servlet.http.HttpServletRequest;
import java.io.File;

public class FileUtils {
    public static String saveFile(HttpServletRequest request, FileItem item, boolean isImage) {
        String absolute = "img/" + (isImage ? "photo" : "audio") + "/" + item.getName();
        String path = request.getServletContext().getRealPath("/tracnghiem");
        try {
            File file = new File(path, absolute);
            if (file.exists()) {
                return absolute;
            }
            file.getParentFile().mkdirs();
            item.write(file);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return absolute;
    }
}
