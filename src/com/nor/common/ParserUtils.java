package com.nor.common;

import org.apache.commons.fileupload.FileItem;

import java.io.IOException;
import java.io.InputStream;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class ParserUtils {
    private static SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");

    public static String getValueOfParam(FileItem fileItem) throws IOException {
        InputStream inputStream = fileItem.getInputStream();
        byte[] b = new byte[1024];
        int count = inputStream.read(b);
        String s = "";
        while (count != -1) {
            s += new String(b, 0, count, "ISO8859_1");
            count = inputStream.read(b);
        }
        inputStream.close();
        return s;
    }

    public static String getCurrentDate() {
        Calendar calendar = Calendar.getInstance();
        return format.format(calendar.getTime());
    }

    public static String parseMoney(String price) {
        return new DecimalFormat("#,###").format(Long.parseLong(price));
    }
}
