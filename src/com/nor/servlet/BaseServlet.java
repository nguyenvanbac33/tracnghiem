package com.nor.servlet;

import com.nor.config.Config;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public abstract class BaseServlet extends HttpServlet {
    protected int result;
    protected String message;
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doLogic(req, resp);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doLogic(req, resp);
    }

    private void doLogic(HttpServletRequest req, HttpServletResponse resp) {
        try {
            message = null;
            result = -2;
            resp.setCharacterEncoding("UTF-8");
            resp.setContentType("text/html; charset=UTF-8");
            req.setCharacterEncoding("UTF-8");
            try {
                handleLogic(req, resp);
            }catch (Exception ex) {
                ex.printStackTrace();
            }
            if (getRoute() == null) return;
            if (result >= -1) {
                req.getSession().setAttribute("result", result);
            }
            if (message != null) {
                req.getSession().setAttribute("message", message);
            }
            resp.sendRedirect(Config.ROOT + getRoute());
        }catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    protected abstract void handleLogic(HttpServletRequest request, HttpServletResponse response) throws Exception;

    protected abstract String getRoute();
}
