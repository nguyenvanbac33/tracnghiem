package com.nor.servlet.admin;

import com.nor.connection.Dao;
import com.nor.model.RegisterCourse;
import com.nor.servlet.BaseServlet;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;

@WebServlet(name = "ConfirmCourseServlet", urlPatterns = {"/confirmCourse"})
public class ConfirmCourseServlet extends BaseServlet {

    @Override
    protected void handleLogic(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String sql = "UPDATE register set status = %s where id = %s";
        if (request.getParameter("confirm") != null) {
            String id = request.getParameter("confirm");
            sql = String.format(sql, "1", id);
        } else {
            String id = request.getParameter("cancel");
            sql = String.format(sql, "-1", id);
        }
        result = Dao.getInstances().query(sql);
    }

    @Override
    protected String getRoute() {
        return "/admin?page=register";
    }
}
