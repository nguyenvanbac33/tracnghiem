package com.nor.servlet.admin;

import com.nor.common.FileUtils;
import com.nor.common.ParserUtils;
import com.nor.connection.Dao;
import com.nor.model.Test;
import com.nor.servlet.BaseServlet;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;

@WebServlet(name = "TestServlet", urlPatterns = {"/admin/test"})
public class TestServlet extends BaseServlet {

    @Override
    protected void handleLogic(HttpServletRequest request, HttpServletResponse response) throws Exception {
        if (request.getParameter("delete") != null) {
            String id = request.getParameter("delete");
            String sql = "delete from `test` where id = '" + id + "'";
            result = Dao.getInstances().query(sql);
            return;
        }
        ServletFileUpload servletFileUpload = new ServletFileUpload(new DiskFileItemFactory());
        Map<String, List<FileItem>> arr = servletFileUpload.parseParameterMap(request);
        if (arr.get("add-row") != null || arr.get("submit-edit-row") != null) {
            String name = ParserUtils.getValueOfParam(arr.get("name").get(0));
            Test test  = new Test();
            test.setName(name);
            test.setIdCourse(request.getSession().getAttribute("idCourse").toString());
            boolean isNew = true;
            if (arr.get("submit-edit-row") != null) {
                String id = ParserUtils.getValueOfParam(arr.get("edit").get(0));
                test.setId(id);
                isNew = false;
            }
            FileItem image = arr.get("images").get(0);
            if (!image.getName().isEmpty()) {
                test.setImage(FileUtils.saveFile(request, arr.get("images").get(0), true));
            }
            if (isNew) {
                result = Dao.getInstances().insert(test);
            } else {
                result = Dao.getInstances().update(test);
            }
        }
    }

    @Override
    protected String getRoute() {
        return "/admin?page=test";
    }
}
