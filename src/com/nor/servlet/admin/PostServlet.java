package com.nor.servlet.admin;

import com.nor.common.FileUtils;
import com.nor.common.ParserUtils;
import com.nor.connection.Dao;
import com.nor.model.Post;
import com.nor.servlet.BaseServlet;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;

@WebServlet(name = "PostServlet", urlPatterns = {"/admin/post"})
public class PostServlet extends BaseServlet {

    @Override
    protected void handleLogic(HttpServletRequest request, HttpServletResponse response) throws Exception {
        if (request.getParameter("delete") != null) {
            String id = request.getParameter("delete");
            String sql = "delete from post where id = '" + id + "'";
            result = Dao.getInstances().query(sql);
            return;
        }
        ServletFileUpload servletFileUpload = new ServletFileUpload(new DiskFileItemFactory());
        Map<String, List<FileItem>> arr = servletFileUpload.parseParameterMap(request);
        try {
            String title = ParserUtils.getValueOfParam(arr.get("title").get(0));
            String shortDesc = ParserUtils.getValueOfParam(arr.get("short_desc").get(0));
            String desc = ParserUtils.getValueOfParam(arr.get("desc").get(0));

            Post post = new Post();
            post.setTitle(title);
            post.setDesc(desc);
            post.setShortDesc(shortDesc);
            Object idCourse = request.getSession().getAttribute("idCourse");
            post.setIdCourse(idCourse == null ? "0" : idCourse.toString());
            boolean isNew = true;
            if (arr.get("submit-edit-row") != null && !arr.get("submit-edit-row").isEmpty()) {
                String id = ParserUtils.getValueOfParam(arr.get("id").get(0));
                post.setId(id);
                String image = ParserUtils.getValueOfParam(arr.get("submit-edit-row").get(0));
                post.setImage(image);
                isNew = false;
            }
            FileItem image = arr.get("images").get(0);
            if (!image.getName().isEmpty()) {
                post.setImage(FileUtils.saveFile(request, arr.get("images").get(0), true));
            }
            if (isNew){
                result = Dao.getInstances().insert(post);
            }else{
                result = Dao.getInstances().update(post);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    protected String getRoute() {
        return "/admin?page=post";
    }
}
