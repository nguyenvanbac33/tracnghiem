package com.nor.servlet.admin;

import com.nor.connection.Dao;
import com.nor.model.Account;
import com.nor.servlet.BaseServlet;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "UserServlet", urlPatterns = {"/admin/user"})
public class UserServlet extends BaseServlet {

    @Override
    protected void handleLogic(HttpServletRequest request, HttpServletResponse response) throws Exception {
        if (request.getParameter("delete") != null) {
            String userName = request.getParameter("delete");
            String sql = "delete from account where username = '"+userName+"'";
            result = Dao.getInstances().query(sql);
        } else if (request.getParameter("submit-edit-row") != null || request.getParameter("add-row") != null) {
            String username = request.getParameter("user_name");
            String password = request.getParameter("password");
            String name = request.getParameter("name");
            String phone = request.getParameter("phone");
            String address = request.getParameter("address");
            Account admin = new Account();
            admin.setAddress(address);
            admin.setName(name);
            admin.setPassword(password);
            admin.setPhone(phone);
            admin.setUserName(username);
            if (request.getParameter("add-row") != null) {
                result = Dao.getInstances().insert(admin);
            } else {
                result = Dao.getInstances().update(admin);
            }
        }
    }

    @Override
    protected String getRoute() {
        return "/admin?page=user";
    }
}
