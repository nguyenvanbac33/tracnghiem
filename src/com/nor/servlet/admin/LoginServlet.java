package com.nor.servlet.admin;

import com.nor.config.Config;
import com.nor.connection.Dao;
import com.nor.model.Account;
import com.nor.servlet.BaseServlet;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;

@WebServlet(name = "LoginServlet", urlPatterns = {"/admin/login"})
public class LoginServlet extends BaseServlet {

    @Override
    protected void handleLogic(HttpServletRequest request, HttpServletResponse response) throws IOException {
        if (request.getParameter("user_name") != null) {
            String userName = request.getParameter("user_name");
            String password = request.getParameter("password");
            String sql = "select * from account where username = '" + userName + "' and password = '" + password + "'";
            ArrayList<Account> arr = Dao.getInstances().getData(Account.class, sql);
            if (!arr.isEmpty()) {
                request.getSession().setAttribute("admin-username", arr.get(0).getUserName());
                request.getSession().setAttribute("name", arr.get(0).getName());
                response.sendRedirect(Config.ROOT +"/admin");
            } else {
                response.sendRedirect(Config.ROOT +"/admin/login.jsp?fail=1");
            }
        } else if (request.getParameter("logout") != null) {
            request.getSession().removeAttribute("name");
            request.getSession().removeAttribute("admin-username");
            response.sendRedirect(Config.ROOT +"/admin/login.jsp");
        } else if (request.getSession().getAttribute("admin-username") != null) {
            response.sendRedirect(Config.ROOT +"/admin");
        }
    }

    @Override
    protected String getRoute() {
        return null;
    }
}
