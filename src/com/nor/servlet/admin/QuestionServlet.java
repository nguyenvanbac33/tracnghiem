package com.nor.servlet.admin;

import com.nor.common.FileUtils;
import com.nor.common.ParserUtils;
import com.nor.connection.Dao;
import com.nor.model.Post;
import com.nor.model.Question;
import com.nor.servlet.BaseServlet;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;

@WebServlet(name = "QuestionServlet", urlPatterns = {"/admin/question"})
public class QuestionServlet extends BaseServlet {
    private String idTest;

    @Override
    protected void handleLogic(HttpServletRequest request, HttpServletResponse response) throws Exception {
        idTest = request.getSession().getAttribute("idTest").toString();
        if (request.getParameter("delete") != null) {
            String id = request.getParameter("delete");
            String sql = "delete from question where id = '" + id + "'";
            result = Dao.getInstances().query(sql);
            return;
        }
        ServletFileUpload servletFileUpload = new ServletFileUpload(new DiskFileItemFactory());
        Map<String, List<FileItem>> arr = servletFileUpload.parseParameterMap(request);
        try {
            String question = ParserUtils.getValueOfParam(arr.get("question").get(0));
            String a = ParserUtils.getValueOfParam(arr.get("a").get(0));
            String b = ParserUtils.getValueOfParam(arr.get("b").get(0));
            String c = ParserUtils.getValueOfParam(arr.get("c").get(0));
            String d = ParserUtils.getValueOfParam(arr.get("d").get(0));
            String answer = ParserUtils.getValueOfParam(arr.get("answer").get(0));
            Question q = new Question();
            q.setQuestion(question);
            q.setIdTest(idTest);
            q.setA(a);
            q.setB(b);
            q.setC(c);
            q.setD(d);
            q.setAnswer(answer);
            boolean isNew = true;
            if (arr.get("submit-edit-row") != null && !arr.get("submit-edit-row").isEmpty()) {
                String id = ParserUtils.getValueOfParam(arr.get("id").get(0));
                q.setId(id);
                isNew = false;
            }
            FileItem image = arr.get("images").get(0);
            if (!image.getName().isEmpty()) {
                q.setImage(FileUtils.saveFile(request, image, true));
            }
            FileItem audio = arr.get("audio").get(0);
            if (!audio.getName().isEmpty()) {
                q.setAudio(FileUtils.saveFile(request, audio, false));
            }
            if (isNew) {
                result = Dao.getInstances().insert(q);
            } else {
                result = Dao.getInstances().update(q);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    protected String getRoute() {
        return "/admin?page=question&id=" + idTest;
    }
}
