package com.nor.servlet.admin;

import com.nor.common.FileUtils;
import com.nor.common.ParserUtils;
import com.nor.connection.Dao;
import com.nor.model.Course;
import com.nor.model.Post;
import com.nor.servlet.BaseServlet;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;

@WebServlet(name = "CourseServlet", urlPatterns = {"/admin/course"})
public class CourseServlet extends BaseServlet {

    @Override
    protected void handleLogic(HttpServletRequest request, HttpServletResponse response) throws Exception {
        if (request.getParameter("delete") != null) {
            String id = request.getParameter("delete");
            String sql = "delete from course where id = '" + id + "'";
            result = Dao.getInstances().query(sql);
            return;
        }
        ServletFileUpload servletFileUpload = new ServletFileUpload(new DiskFileItemFactory());
        Map<String, List<FileItem>> arr = servletFileUpload.parseParameterMap(request);
        try {
            String name = ParserUtils.getValueOfParam(arr.get("name").get(0));
            String summary = ParserUtils.getValueOfParam(arr.get("summary").get(0));
            String desc = ParserUtils.getValueOfParam(arr.get("desc").get(0));
            String duration = ParserUtils.getValueOfParam(arr.get("duration").get(0));
            String price = ParserUtils.getValueOfParam(arr.get("price").get(0));

            Course course = new Course();
            course.setDesc(desc);
            course.setDuration(duration);
            course.setName(name);
            course.setPrice(price);
            course.setSummary(summary);
            boolean isNew = true;
            if (arr.get("submit-edit-row") != null && !arr.get("submit-edit-row").isEmpty()) {
                String id = ParserUtils.getValueOfParam(arr.get("id").get(0));
                course.setId(id);
                isNew = false;
            }
            FileItem image = arr.get("images").get(0);
            if (!image.getName().isEmpty()) {
                course.setImage(FileUtils.saveFile(request, arr.get("images").get(0), true));
            }
            if (isNew){
                result = Dao.getInstances().insert(course);
            }else{
                result = Dao.getInstances().update(course);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    protected String getRoute() {
        return "/admin?page=course";
    }
}
