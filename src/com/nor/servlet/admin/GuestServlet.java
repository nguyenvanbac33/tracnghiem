package com.nor.servlet.admin;

import com.nor.connection.Dao;
import com.nor.servlet.BaseServlet;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "GuestServlet", urlPatterns = {"/admin/guest"})
public class GuestServlet extends BaseServlet {

    @Override
    protected void handleLogic(HttpServletRequest request, HttpServletResponse response) throws IOException {
        if (request.getParameter("delete") != null) {
            String userName = request.getParameter("delete");
            String sql = "delete from account where username = '"+userName+"'";
            result = Dao.getInstances().query(sql);
            return;
        }
    }

    @Override
    protected String getRoute() {
        return "/admin?page=guest";
    }
}
