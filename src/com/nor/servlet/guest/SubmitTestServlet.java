package com.nor.servlet.guest;

import com.nor.connection.Dao;
import com.nor.model.Question;
import com.nor.servlet.BaseServlet;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;

@WebServlet(name = "SubmitTestServlet", urlPatterns = {"/submitTest"})
public class SubmitTestServlet extends BaseServlet {
    private String route;

    @Override
    protected void handleLogic(HttpServletRequest request, HttpServletResponse response) throws IOException {
        route = null;
        String idTest = request.getParameter("submit");
        if (idTest != null) {
            String sql = "SELECT * FROM question WHERE id_test = " + idTest;
            ArrayList<Question> questions = Dao.getInstances().getData(Question.class, sql);
            String username = request.getSession().getAttribute("username").toString();
            for (Question q : questions) {
                String answer = request.getParameter(q.getId());
                sql = "INSERT INTO answer values(" + q.getId() + ", '" + answer + "', '" + username + "', " + idTest + ")";
                Dao.getInstances().query(sql);
            }
        }
        route = "/single-test.jsp?id=" + idTest;
    }

    @Override
    protected String getRoute() {
        return route;
    }
}
