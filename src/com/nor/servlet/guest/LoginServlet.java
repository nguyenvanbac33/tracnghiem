package com.nor.servlet.guest;

import com.nor.config.Config;
import com.nor.connection.Dao;
import com.nor.model.Account;
import com.nor.servlet.BaseServlet;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;

@WebServlet(name = "LoginGuestServlet", urlPatterns = {"/login"})
public class LoginServlet extends BaseServlet {
    private String route;
    @Override
    protected void handleLogic(HttpServletRequest request, HttpServletResponse response) throws IOException {
        route = null;
        if (request.getParameter("logout") != null) {
            request.getSession().removeAttribute("name");
            request.getSession().removeAttribute("username");
            response.sendRedirect(Config.ROOT);
            return;
        }

        if (request.getParameter("register") != null) {
            String password = request.getParameter("password");
            String name = request.getParameter("name");
            String phone = request.getParameter("phone");
            String address = request.getParameter("address");
            String email = request.getParameter("email");

            Account account = new Account();
            account.setAddress(address);
            account.setUserName(email);
            account.setName(name);
            account.setPassword(password);
            account.setPhone(phone);
            account.setType("1");
            result = Dao.getInstances().insert(account);
            route = "/login.jsp";
        } else if (request.getParameter("login") != null) {
            String username = request.getParameter("username");
            String password = request.getParameter("password");
            String sql = "SELECT * FROM account where username = '" + username + "' and password = '" + password + "'";
            ArrayList<Account> guests = Dao.getInstances().getData(Account.class, sql);
            if (guests.size() > 0) {
                request.getSession().setAttribute("name", guests.get(0).getUserName());
                request.getSession().setAttribute("username", guests.get(0).getName());
                route = "/";
            } else {
                result = -1;
                route = "/login.jsp";
            }
        }
    }

    @Override
    protected String getRoute() {
        return route;
    }
}
