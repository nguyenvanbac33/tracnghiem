package com.nor.servlet.guest;

import com.nor.connection.Dao;
import com.nor.model.Comment;
import com.nor.model.RegisterCourse;
import com.nor.servlet.BaseServlet;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;

@WebServlet(name = "CommentPostServlet", urlPatterns = {"/comment"})
public class CommentPostServlet extends BaseServlet {
    private String route;

    @Override
    protected void handleLogic(HttpServletRequest request, HttpServletResponse response) throws IOException {
        route = null;
        if (request.getSession().getAttribute("username") == null) {
            message = "Bạn phải đăng nhập trước";
            route = "/login.jsp";
            return;
        }
        String username = request.getSession().getAttribute("username").toString();
        String idPost = "";
        if (request.getParameter("comment") != null) {
            idPost = request.getParameter("comment");
            String content = request.getParameter("content");
            Comment comment = new Comment();
            comment.setContent(content);
            comment.setUsername(username);
            comment.setIdPost(idPost);
            Dao.getInstances().insert(comment);
        }
        route = "/single-blog.jsp?id=" + idPost;
    }

    @Override
    protected String getRoute() {
        return route;
    }
}
