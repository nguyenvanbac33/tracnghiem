package com.nor.servlet.guest;

import com.nor.config.Config;
import com.nor.connection.Dao;
import com.nor.model.Account;
import com.nor.model.RegisterCourse;
import com.nor.servlet.BaseServlet;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;

@WebServlet(name = "RegisterCourseServlet", urlPatterns = {"/registerCourse"})
public class RegisterCourseServlet extends BaseServlet {
    private String route;

    @Override
    protected void handleLogic(HttpServletRequest request, HttpServletResponse response) throws IOException {
        route = null;
        if (request.getSession().getAttribute("username") == null) {
            message = "Bạn phải đăng nhập trước";
            route = "/login.jsp";
            return;
        }
        String idCourse = request.getParameter("id");
        String username = request.getSession().getAttribute("username").toString();
        ArrayList<RegisterCourse> registerCourses = Dao.getInstances().getData(RegisterCourse.class, "SELECT * FROM register where id_course = " + idCourse + " and email = '" + username + "'");
        if (registerCourses.isEmpty()) {
            RegisterCourse registerCourse = new RegisterCourse();
            registerCourse.setIdCourse(idCourse);
            registerCourse.setMail(username);
            registerCourse.setStatus("0");
            result = Dao.getInstances().insert(registerCourse);
            route = "/single-course.jsp?id=" + idCourse;
        } else {
            RegisterCourse registerCourse = registerCourses.get(0);
            registerCourse.setStatus("-1");
            result = Dao.getInstances().update(registerCourse);
            route = "/single-course.jsp?id=" + idCourse;
        }
    }

    @Override
    protected String getRoute() {
        return route;
    }
}
