package com.nor.connection;

import com.nor.model.BaseModel;
import com.nor.model.TableInfo;

import java.lang.reflect.Field;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

public class Dao {

    private static Dao dao;
    private Connection mConnection;

    private Dao() {
    }

    public static Dao getInstances() {
        if (dao == null) {
            dao = new Dao();
        }
        return dao;
    }

    private Connection getConnection() {
        if (mConnection == null) {
            try {
                openConnect();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return mConnection;
    }

    public void openConnect() throws Exception {
        Class.forName("com.mysql.cj.jdbc.Driver");
        mConnection = DriverManager.getConnection("jdbc:mysql://localhost:3306/ql_tracnghiem?useUnicode=yes&characterEncoding=UTF-8&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC", "root", "");
    }

    public <T extends BaseModel> ArrayList<T> getData(Class<T> clz, String sql) {
        System.out.println(sql);
        ArrayList<T> arr = new ArrayList<>();
        try {
            Statement statement = getConnection().createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,
                    ResultSet.CONCUR_READ_ONLY);
            ResultSet rs = statement.executeQuery(sql);
            rs.first();
            while (!rs.isAfterLast() && !rs.wasNull()) {
                T t = clz.getConstructor().newInstance();
                Field[] fields = clz.getDeclaredFields();
                for (Field f : fields) {
                    TableInfo info = f.getAnnotation(TableInfo.class);
                    if (info == null) continue;
                    f.setAccessible(true);
                    try {
                        String value = rs.getString(info.columnName());
                        f.set(t, value);
                    } catch (Exception ex) {
                    }
                }
                arr.add(t);
                rs.next();
            }
        } catch (Exception ex) {
         //   ex.printStackTrace();
        }
        return arr;
    }

    public int update(BaseModel model) {
        return query(model.sqlUpdate());
    }


    public int insert(BaseModel model) {
        return query(model.sqlInsert());
    }

    public int query(String sql) {
        System.out.println(sql);
        try {
            Statement statement = getConnection().createStatement();
            int result = statement.executeUpdate(sql);
            return result;
        } catch (Exception ex) {
            ex.printStackTrace();
            return -1;
        }
    }
}
