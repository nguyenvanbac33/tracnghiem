package com.nor.model;

@TableInfo(tableName = "account")
public class Account extends BaseModel{
    @TableInfo(columnName = "username", primaryKey = true)
    private String userName;
    @TableInfo(columnName = "name")
    private String name;
    @TableInfo(columnName = "password")
    private String password;
    @TableInfo(columnName = "address")
    private String address;
    @TableInfo(columnName = "phone")
    private String phone;
    @TableInfo(columnName = "type")
    private String type = "0";

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
