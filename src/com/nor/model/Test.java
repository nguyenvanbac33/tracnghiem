package com.nor.model;

import java.text.SimpleDateFormat;
import java.util.Date;

@TableInfo(tableName = "`test`")
public class Test extends BaseModel{

    @TableInfo(columnName = "id", primaryKey = true)
    private String id = System.currentTimeMillis()+"";

    @TableInfo(columnName = "name", needEncode = true)
    private String name;

    @TableInfo(columnName = "id_course")
    private String idCourse;

    @TableInfo(columnName = "pub_date")
    private String pubDate =  new SimpleDateFormat("dd/MM/yyyy").format(new Date());;

    @TableInfo(columnName = "image")
    private String image;

    public String getIdCourse() {
        return idCourse;
    }

    public void setIdCourse(String idCourse) {
        this.idCourse = idCourse;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPubDate() {
        return pubDate;
    }

    public void setPubDate(String pubDate) {
        this.pubDate = pubDate;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
