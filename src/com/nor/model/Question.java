package com.nor.model;

@TableInfo(tableName = "`question`")
public class Question extends BaseModel{

    @TableInfo(columnName = "id", primaryKey = true)
    private String id = System.currentTimeMillis()+"";

    @TableInfo(columnName = "id_test")
    private String idTest;

    @TableInfo(columnName = "question", needEncode = true)
    private String question;

    @TableInfo(columnName = "a", needEncode = true)
    private String a;

    @TableInfo(columnName = "b", needEncode = true)
    private String b;

    @TableInfo(columnName = "c", needEncode = true)
    private String c;

    @TableInfo(columnName = "d", needEncode = true)
    private String d;

    @TableInfo(columnName = "answer", needEncode = true)
    private String answer;

    @TableInfo(columnName = "image")
    private String image;

    @TableInfo(columnName = "audio")
    private String audio;

    @TableInfo(columnName = "user_answer", unique = true)
    private String userAnswer;

    public String getUserAnswer() {
        return userAnswer;
    }

    public void setUserAnswer(String userAnswer) {
        this.userAnswer = userAnswer;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdTest() {
        return idTest;
    }

    public void setIdTest(String idTest) {
        this.idTest = idTest;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getA() {
        return a;
    }

    public void setA(String a) {
        this.a = a;
    }

    public String getB() {
        return b;
    }

    public void setB(String b) {
        this.b = b;
    }

    public String getC() {
        return c;
    }

    public void setC(String c) {
        this.c = c;
    }

    public String getD() {
        return d;
    }

    public void setD(String d) {
        this.d = d;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getAudio() {
        return audio;
    }

    public void setAudio(String audio) {
        this.audio = audio;
    }
}
