package com.nor.model;

import java.text.SimpleDateFormat;
import java.util.Date;

@TableInfo(tableName = "`register`")
public class RegisterCourse extends BaseModel{

    @TableInfo(columnName = "id", primaryKey = true)
    private String id = System.currentTimeMillis()+"";

    @TableInfo(columnName = "id_course", needEncode = true)
    private String idCourse;

    @TableInfo(columnName = "status", needEncode = true)
    private String status;

    @TableInfo(columnName = "email", needEncode = true)
    private String mail;

    @TableInfo(columnName = "pub_date")
    private String pubDate =  new SimpleDateFormat("dd/MM/yyyy").format(new Date());

    @TableInfo(columnName = "name", unique = true)
    private String name;
    @TableInfo(columnName = "address", unique = true)
    private String address;
    @TableInfo(columnName = "phone", unique = true)
    private String phone;
    @TableInfo(columnName = "course", unique = true)
    private String course;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdCourse() {
        return idCourse;
    }

    public void setIdCourse(String idCourse) {
        this.idCourse = idCourse;
    }

    public String getStatus() {
        return status;
    }

    public String getStatusDisplay() {
        switch (status) {
            case "-1":
                return "Đã hủy";
            case "0":
                return "Đang đăng ký";
            default:
                return "Đã đăng ký";
        }
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getPubDate() {
        return pubDate;
    }

    public void setPubDate(String pubDate) {
        this.pubDate = pubDate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getCourse() {
        return course;
    }

    public void setCourse(String course) {
        this.course = course;
    }
}
