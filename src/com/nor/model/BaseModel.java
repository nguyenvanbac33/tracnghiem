package com.nor.model;

import java.lang.reflect.Field;

public abstract class BaseModel {
    public String sqlInsert() {
        try {
            TableInfo info = getClass().getAnnotation(TableInfo.class);
            String tableName = info.tableName();
            String sql = "INSERT INTO " + tableName;
            String columns = "";
            String values = "";
            Field[] fields = getClass().getDeclaredFields();
            for (Field f : fields) {
                f.setAccessible(true);
                info = f.getAnnotation(TableInfo.class);
                if (info == null || info.unique()) continue;
                columns += "`" + info.columnName() + "`,";
                String value = f.get(this) == null ? "" : f.get(this).toString();
                if (info.needEncode()) {
                    value = new String(value.getBytes("ISO8859_1"), "UTF8");
                }
                values += "'" + value + "',";
            }
            columns = columns.substring(0, columns.length() - 1);
            values = values.substring(0, values.length() - 1);
            sql += " (" + columns + ") values (" + values + ")";
            return sql;
        } catch (Exception ex) {
            ex.printStackTrace();
            return "";
        }
    }

    public String sqlUpdate() {
        try {
            TableInfo info = getClass().getAnnotation(TableInfo.class);
            String tableName = info.tableName();
            String sql = "UPDATE " + tableName + " SET ";
            Field[] fields = getClass().getDeclaredFields();
            Field key = null;
            for (Field f : fields) {
                f.setAccessible(true);
                if (f.get(this) == null) continue;
                info = f.getAnnotation(TableInfo.class);
                if (info == null || info.unique()) continue;
                if (info.primaryKey()) {
                    key = f;
                }
                String value = f.get(this).toString();
                if (info.needEncode()) {
                    value = new String(value.getBytes("ISO8859_1"), "UTF8");
                }
                sql += "`" + info.columnName() + "`='" + value + "',";
            }
            sql = sql.substring(0, sql.length() - 1);
            info = key.getAnnotation(TableInfo.class);
            sql += " WHERE " + info.columnName() + " = '" + key.get(this) + "'";
            return sql;
        } catch (Exception ex) {
            return "";
        }
    }
}
