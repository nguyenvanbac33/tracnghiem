package com.nor.model;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
public @interface TableInfo {

    String tableName() default "";
    String columnName() default  "";
    boolean primaryKey() default false;
    boolean unique() default false;
    boolean needEncode() default false;
}
