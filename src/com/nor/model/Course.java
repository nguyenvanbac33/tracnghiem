package com.nor.model;

@TableInfo(tableName = "`course`")
public class Course extends BaseModel{

    @TableInfo(columnName = "id", primaryKey = true)
    private String id = System.currentTimeMillis()+"";

    @TableInfo(columnName = "name", needEncode = true)
    private String name;

    @TableInfo(columnName = "summary", needEncode = true)
    private String summary;

    @TableInfo(columnName = "desc", needEncode = true)
    private String desc;

    @TableInfo(columnName = "price")
    private String price;

    @TableInfo(columnName = "duration", needEncode = true)
    private String duration;

    @TableInfo(columnName = "image", needEncode = true)
    private String image;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
