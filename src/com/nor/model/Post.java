package com.nor.model;

import java.text.SimpleDateFormat;
import java.util.Date;

@TableInfo(tableName = "post")
public class Post extends BaseModel{
    @TableInfo(columnName = "id", primaryKey = true)
    private String id = System.currentTimeMillis()+"";
    @TableInfo(columnName = "title", needEncode = true)
    private String title;
    @TableInfo(columnName = "short_desc", needEncode = true)
    private String shortDesc;
    @TableInfo(columnName = "desc", needEncode = true)
    private String desc;
    @TableInfo(columnName = "image")
    private String image;
    @TableInfo(columnName = "pub_date")
    private String pubDate =  new SimpleDateFormat("dd/MM/yyyy").format(new Date());
    @TableInfo(columnName = "id_course")
    private String idCourse;

    public String getIdCourse() {
        return idCourse;
    }

    public void setIdCourse(String idCourse) {
        this.idCourse = idCourse;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getShortDesc() {
        return shortDesc;
    }

    public void setShortDesc(String shortDesc) {
        this.shortDesc = shortDesc;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getPubDate() {
        return pubDate;
    }

    public void setPubDate(String pubDate) {
        this.pubDate = pubDate;
    }
}
