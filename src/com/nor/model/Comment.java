package com.nor.model;

import java.text.SimpleDateFormat;
import java.util.Date;

@TableInfo(tableName = "comment")
public class Comment extends BaseModel{
    @TableInfo(columnName = "id", primaryKey = true)
    private String id = System.currentTimeMillis()+"";
    @TableInfo(columnName = "id_post")
    private String idPost;
    @TableInfo(columnName = "username")
    private String username;
    @TableInfo(columnName = "content")
    private String content;
    @TableInfo(columnName = "pub_date")
    private String pubDate = new SimpleDateFormat("dd/MM/yyyy").format(new Date());

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdPost() {
        return idPost;
    }

    public void setIdPost(String idPost) {
        this.idPost = idPost;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getPubDate() {
        return pubDate;
    }

    public void setPubDate(String pubDate) {
        this.pubDate = pubDate;
    }
}
