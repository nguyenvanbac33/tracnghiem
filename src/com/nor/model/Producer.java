package com.nor.model;

@TableInfo(tableName = "producer")
public class Producer extends BaseModel{
    @TableInfo(columnName = "id", primaryKey = true)
    private String id = System.currentTimeMillis()+"";
    @TableInfo(columnName = "name")
    private String name;
    @TableInfo(columnName = "phone")
    private String phone;
    @TableInfo(columnName = "address")
    private String address;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
