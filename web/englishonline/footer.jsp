<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!-- Footer section -->
<footer class="footer-section">
    <div class="container footer-top">
        <div class="row">
            <!-- widget -->
            <div class="col-sm-6 col-lg-6 footer-widget">
                <div class="about-widget">
                    <img src="img/logo-light.png" alt="">
                    <p>Tiếng anh chỉ là chuyện nhỏ, biết điều không thể thành có thể</p>
                    <div class="social pt-1">
                        <a href=""><i class="fa fa-twitter-square"></i></a>
                        <a href=""><i class="fa fa-facebook-square"></i></a>
                        <a href=""><i class="fa fa-google-plus-square"></i></a>
                        <a href=""><i class="fa fa-linkedin-square"></i></a>
                        <a href=""><i class="fa fa-rss-square"></i></a>
                    </div>
                </div>
            </div>
            <!-- widget -->
            <div class="col-sm-6 col-lg-6 footer-widget">
                <h6 class="fw-title">CONTACT</h6>
                <ul class="contact">
                    <li><p><i class="fa fa-map-marker"></i> 40 Xuan Thuy, Cau Giay, Ha Noi</p></li>
                    <li><p><i class="fa fa-phone"></i> (+88) 111 555 666</p></li>
                    <li><p><i class="fa fa-envelope"></i> unica@gmail.com</p></li>
                    <li><p><i class="fa fa-clock-o"></i> Monday - Friday, 08:00AM - 06:00 PM</p></li>
                </ul>
            </div>
        </div>
    </div>
</footer>
<!-- Footer section end-->



<!--====== Javascripts & Jquery ======-->
<script src="js/jquery-3.2.1.min.js"></script>
<script src="js/owl.carousel.min.js"></script>
<script src="js/jquery.countdown.js"></script>
<script src="js/masonry.pkgd.min.js"></script>
<script src="js/magnific-popup.min.js"></script>
<script src="js/main.js"></script>

</body>
</html>
