<%@ page import="com.nor.connection.Dao" %>
<%@ page import="com.nor.model.Post" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<div id="update-row" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Sửa bài viết</h4>
            </div>
            <div class="modal-body">
                <form method="post" enctype="multipart/form-data" action="/admin/post">
                    <%
                        if (request.getParameter("edit") != null) {
                            String id = request.getParameter("edit");
                            String sql = "select * from post where id = '" + id + "'";
                            Post item = Dao.getInstances().getData(Post.class, sql).get(0);
                    %>

                    <div class="form-group">
                        <label>Mã bài viết</label> <input
                            type="text" maxlength="100" class="form-control add-control" value="<%= item.getId()%>"
                            name="id" readOnly>
                    </div>
                    <div class="form-group">
                        <label>Tiêu đề</label> <input
                            type="text" maxlength="100" class="form-control add-control" value="<%= item.getTitle()%>"
                            name="title" required>
                    </div>
                    <div class="form-group">
                        <label>Mô tả</label>
                        <br/>
                        <input type="text" maxlength="200" class="form-control add-control"
                               value="<%= item.getShortDesc()%>" required name="short_desc">
                    </div>
                    <div class="form-group">
                        <label>Chi tiết</label>
                        <br/>
                        <textarea name="desc" id="summary-edit" required><%= item.getDesc()%></textarea>
                        <script type="text/javascript">CKEDITOR.replace('summary-edit'); </script>
                    </div>
                    <div class="form-group">
                        <label>Hình ảnh</label>
                    </div>
                    <input accept="image/*" type="file" name="images"/>

                    <div class="form-group" style="text-align: right;">
                        <button type="submit" class="btn btn-primary" name="submit-edit-row"
                                value="<%= item.getImage()%>">Ok
                        </button>
                    </div>
                    <script type="text/javascript">
                        $('#update-row').modal('show');
                    </script>
                </form>
                <%
                    }
                %>
            </div>
        </div>
    </div>
</div>
