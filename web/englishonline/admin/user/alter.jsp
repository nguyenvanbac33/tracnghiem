<%@ page import="com.nor.model.Account" %>
<%@ page import="com.nor.connection.Dao" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<div id="update-row" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Cập nhập quản trị</h4>
            </div>
            <div class="modal-body">
                <form method="post" action="/admin/user">
                    <%
                        if (request.getParameter("edit") != null) {
                            String id = request.getParameter("edit");
                            String sql = "select * from account where username = '" + id + "'";
                            Account admin = Dao.getInstances().getData(Account.class, sql).get(0);
                    %>
                    <div class="form-group">
                        <label>Tên đăng nhập</label> <input
                            type="text" class="form-control add-control" value="<% out.println(admin.getUserName());%>"
                            readonly="readonly" name="user_name" required>
                    </div>
                    <div class="form-group">
                        <label>Mật khẩu</label> <input
                            type="password" class="form-control add-control" value="<% out.println(admin.getPassword());%>"
                            name="password" required>
                    </div>
                    <div class="form-group">
                        <label>Họ tên</label> <input
                            type="text" class="form-control add-control" name="name" value="<% out.println(admin.getName());%>"
                            required>
                    </div>
                    <div class="form-group">
                        <label>Địa chỉ</label> <input
                            type="text" class="form-control add-control" name="address"
                            value="<% out.println(admin.getAddress());%>" required>
                    </div>
                    <div class="form-group">
                        <label>Số điện thoại</label> <input
                            type="text" class="form-control add-control" name="phone"
                            value="<% out.println(admin.getPhone());%>" required>
                    </div>
                    <div class="form-group" style="text-align: right;">
                        <input type="submit" class="btn btn-primary" name="submit-edit-row" value="Ok"/>
                    </div>
                    <script type="text/javascript">
                        $('#update-row').modal('show');
                    </script>
                    <%
						}
					%>
                </form>
            </div>
        </div>

    </div>
</div>
