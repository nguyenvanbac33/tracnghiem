<%@ page import="java.util.ArrayList" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%
    String currentPage = request.getParameter("page");
    currentPage = currentPage == null ? "user" : currentPage;
%>
<li class="nav-item
<%
 	if (currentPage.equals("user")){
 	    out.println("active");
 	}
 %>">
    <a class="nav-link" href="?page=user">
        <i class="material-icons">person</i>
        <p>Quản trị</p>
    </a>
</li>
<li class="nav-item
<%
 	if (currentPage.equals("guest")){
 	    out.println("active");
 	}
 %>">
    <a class="nav-link" href="?page=guest">
        <i class="material-icons">group</i>
        <p>Sinh viên</p>
    </a>
</li>
<li class="nav-item
<%
    ArrayList<String> pages = new ArrayList<>();
    pages.add("course");
    pages.add("post");
    pages.add("test");
 	if (pages.contains(currentPage)){
 	    out.println("active");
 	}
 %>">
    <a class="nav-link" href="?page=course">
        <i class="material-icons">group</i>
        <p>Khóa học</p>
    </a>
</li>
<li class="nav-item
<%
 	if (currentPage.equals("blog")){
 	    out.println("active");
 	}
 %>">
    <a class="nav-link" href="?page=blog">
        <i class="material-icons">group</i>
        <p>Bài viết</p>
    </a>
</li>
<li class="nav-item
<%
 	if (currentPage.equals("register")){
 	    out.println("active");
 	}
 %>">
    <a class="nav-link" href="?page=register">
        <i class="material-icons">group</i>
        <p>Đăng ký</p>
    </a>
</li>
<li class="nav-item">
    <a class="nav-link" href="/admin/login?logout=logout">
        <i class="material-icons">exit_to_app</i>
        <p>Đăng xuất</p>
    </a>
</li>
