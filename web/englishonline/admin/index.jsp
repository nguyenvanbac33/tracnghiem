<jsp:include page="header.jsp"/>
<%
    String currentPage = request.getParameter("page");
    currentPage = currentPage == null ? "user" : currentPage;
%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<body class="">
<%
    if (request.getSession().getAttribute("result") != null) {
        int result = Integer.parseInt(request.getSession().getAttribute("result").toString());
        request.getSession().removeAttribute("result");
        if (result > 0) {
            out.println("<script type='text/javascript'>alert('Success');</script>");
        } else {
            out.println("<script type='text/javascript'>alert('Fail');</script>");
        }
    }
%>
<div class="wrapper ">
    <div class="sidebar" data-color="purple">
        <div class="logo" style="background: #fff">
            <a href="/englishonline/admin" class="simple-text logo-normal">
                <%
                    out.println(session.getAttribute("name"));
                %>
            </a>
        </div>
        <div class="sidebar-wrapper" style="background: #fff">
            <ul class="nav">
                <jsp:include page="menu.jsp"/>
            </ul>
        </div>
    </div>
    <div class="main-panel">
        <!-- Navbar -->
        <nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top ">
            <div class="container-fluid">
                <button class="navbar-toggler" type="button" data-toggle="collapse" aria-controls="navigation-index"
                        aria-expanded="false" aria-label="Toggle navigation">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="navbar-toggler-icon icon-bar"></span>
                    <span class="navbar-toggler-icon icon-bar"></span>
                    <span class="navbar-toggler-icon icon-bar"></span>
                </button>
                <div class="collapse navbar-collapse justify-content-end">
                    <form class="navbar-form" method="post">
                        <div class="input-group no-border">
                            <input type="text" value="<%
                            String key = request.getParameter("key");
                            key = key == null ? "" : new String(key.getBytes("ISO8859_1"), "UTF8");
                            out.println(key);
                            %>" name="key" class="input-search form-control"
                                   placeholder="Search...">
                            <button type="submit" style="margin-left: 10px; background-color: #8e24aa; color: white"
                                    class="btn btn-white btn-round btn-just-icon">
                                <i class="material-icons">search</i>
                                <div class="ripple-container"></div>
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </nav>
        <!-- End Navbar -->
        <div class="content">
            <div class="container-fluid">

                <div class="card">
                    <div>
                        <%
                            switch (currentPage) {
                                case "guest":
                                case "register":
                                    break;
                                default:
                                    out.println("<button type='button' id='add' class='card-header-primary btn' data-toggle='modal' data-target='#insert'>Add</button>");
                                    break;
                            }
                        %>
                    </div>
                    <div class="card-body">
                        <%
                            switch (currentPage) {
                                case "user":
                        %>
                        <jsp:include page="user/index.jsp"/>
                        <%
                                break;
                            case "guest":
                        %>
                        <jsp:include page="guest/index.jsp"/>
                        <%
                                break;
                            case "post":
                        %>
                        <jsp:include page="post/index.jsp"/>
                        <%
                                break;
                            case "blog":
                        %>
                        <jsp:include page="blog/index.jsp"/>
                        <%
                                break;
                            case "test":
                        %>
                        <jsp:include page="test/index.jsp"/>
                        <%
                                break;
                            case "question":
                        %>
                        <jsp:include page="question/index.jsp"/>
                        <%
                                break;
                            case "register":
                        %>
                        <jsp:include page="register/index.jsp"/>
                        <%
                                break;
                            case "course":
                        %>
                        <jsp:include page="course/index.jsp"/>
                        <%
                                    break;
                            }
                        %>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</body>
</html>
