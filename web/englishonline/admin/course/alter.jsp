<%@ page import="com.nor.connection.Dao" %>
<%@ page import="com.nor.model.Question" %>
<%@ page import="com.nor.model.Course" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<div id="update-row" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Sửa bài viết</h4>
            </div>
            <div class="modal-body">
                <form id="fr-add-alphabet" method="post" enctype="multipart/form-data"
                      action="/admin/course">
                    <%
                        if (request.getParameter("edit") != null) {
                            String id = request.getParameter("edit");
                            String sql = "select * from course where id = '" + id + "'";
                            Course item = Dao.getInstances().getData(Course.class, sql).get(0);
                    %>

                    <div class="form-group">
                        <label>Mã khóa học</label> <input
                            type="text" maxlength="100" class="form-control add-control" value="<%= item.getId()%>"
                            name="id" readOnly>
                    </div>
                    <div class="form-group">
                        <label>Tên khóa học</label>
                        <br/>
                        <input type="text" class="form-control add-control" required value="<%= item.getName()%>" name="name">
                    </div>
                    <div class="form-group">
                        <label>Thời gian</label>
                        <br/>
                        <input type="text" class="form-control add-control" required value="<%= item.getDuration()%>" name="duration">
                    </div>
                    <div class="form-group">
                        <label>Giá</label>
                        <br/>
                        <input type="number" class="form-control add-control" required value="<%= item.getPrice()%>" name="price">
                    </div>
                    <div class="form-group">
                        <label>Mô tả</label>
                        <br/>
                        <input type="text" class="form-control add-control" required value="<%= item.getSummary()%>" name="summary">
                    </div>
                    <div class="form-group">
                        <label>Nội dung</label>
                        <br/>
                        <textarea name="desc" id="desc-edit" required><%= item.getDesc()%></textarea>
                        <script>CKEDITOR.replace('desc-edit'); </script>
                    </div>
                    <label>Hình ảnh</label>
                    <br/>
                    <input accept="image/*" type="file" name="images"/>
                    <div class="form-group" style="text-align: right;">
                        <button type="submit" class="btn btn-primary" name="submit-edit-row"
                                value="<%= item.getImage()%>">Ok
                        </button>
                    </div>
                    <script type="text/javascript">
                        $('#update-row').modal('show');
                    </script>
                </form>
                <%
                    }
                %>
            </div>
        </div>
    </div>
</div>
