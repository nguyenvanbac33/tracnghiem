<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<div id="insert" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Thêm câu hỏi</h4>
            </div>
            <div class="modal-body">
                <form id="fr-add-alphabet" method="post" enctype="multipart/form-data"
                      action="/admin/question">
                    <div class="form-group">
                        <label>Câu hỏi</label>
                        <br/>
                        <input type="text" class="form-control add-control" required name="question">
                    </div>
                    <div class="form-group">
                        <label>A</label>
                        <br/>
                        <input type="text" class="form-control add-control" required name="a">
                    </div>
                    <div class="form-group">
                        <label>B</label>
                        <br/>
                        <input type="text" class="form-control add-control" required name="b">
                    </div>
                    <div class="form-group">
                        <label>C</label>
                        <br/>
                        <input type="text" class="form-control add-control" required name="c">
                    </div>
                    <div class="form-group">
                        <label>D</label>
                        <br/>
                        <input type="text" class="form-control add-control" required name="d">
                    </div>
                    <div class="form-group">
                        <label>Câu trả lời đúng</label>
                        <br/>
                        <input type="text" class="form-control add-control" required name="answer">
                    </div>
                    <label>Hình ảnh</label>
                    <br/>
                    <input accept="image/*" type="file" name="images"/>
                    <label>Âm thanh</label>
                    <br/>
                    <input accept="audio/*" type="file" name="audio"/>
                    <div class="form-group" style="text-align: right;">
                        <input type="submit" class="btn btn-primary" name="add-new-row" value="Ok"/>
                    </div>
                </form>
            </div>
        </div>

    </div>
</div>
