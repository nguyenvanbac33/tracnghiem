<%@ page import="com.nor.connection.Dao" %>
<%@ page import="com.nor.model.Question" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<div id="update-row" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Sửa bài viết</h4>
            </div>
            <div class="modal-body">
                <form id="fr-add-alphabet" method="post" enctype="multipart/form-data"
                      action="/admin/question">
                    <%
                        if (request.getParameter("edit") != null) {
                            String id = request.getParameter("edit");
                            String sql = "select * from question where id = '" + id + "'";
                            Question item = Dao.getInstances().getData(Question.class, sql).get(0);
                    %>

                    <div class="form-group">
                        <label>Mã câu hỏi</label> <input
                            type="text" maxlength="100" class="form-control add-control" value="<%= item.getId()%>"
                            name="id" readOnly>
                    </div>
                    <div class="form-group">
                        <label>Câu hỏi</label>
                        <br/>
                        <input type="text" class="form-control add-control" required value="<%= item.getQuestion()%>" name="question">
                    </div>
                    <div class="form-group">
                        <label>A</label>
                        <br/>
                        <input type="text" class="form-control add-control" required value="<%= item.getA()%>" name="a">
                    </div>
                    <div class="form-group">
                        <label>B</label>
                        <br/>
                        <input type="text" class="form-control add-control" required value="<%= item.getB()%>" name="b">
                    </div>
                    <div class="form-group">
                        <label>C</label>
                        <br/>
                        <input type="text" class="form-control add-control" required value="<%= item.getC()%>" name="c">
                    </div>
                    <div class="form-group">
                        <label>D</label>
                        <br/>
                        <input type="text" class="form-control add-control" required value="<%= item.getD()%>" name="d">
                    </div>
                    <div class="form-group">
                        <label>Câu trả lời đúng</label>
                        <br/>
                        <input type="text" class="form-control add-control" required name="answer" value="<%= item.getAnswer()%>">
                    </div>
                    <label>Hình ảnh</label>
                    <br/>
                    <input accept="image/*" type="file" name="images"/>
                    <label>Âm thanh</label>
                    <br/>
                    <input accept="audio/*" type="file" name="audio"/>

                    <div class="form-group" style="text-align: right;">
                        <button type="submit" class="btn btn-primary" name="submit-edit-row"
                                value="<%= item.getImage()%>">Ok
                        </button>
                    </div>
                    <script type="text/javascript">
                        $('#update-row').modal('show');
                    </script>
                </form>
                <%
                    }
                %>
            </div>
        </div>
    </div>
</div>
