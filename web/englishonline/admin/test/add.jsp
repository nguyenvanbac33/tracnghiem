<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<div id="insert" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Thêm đề thi</h4>
			</div>
			<div class="modal-body">
				<form method="post" action="/admin/test" enctype="multipart/form-data">
					<div class="form-group">
						<label>Tên đề thi</label> <input
							type="text" class="form-control add-control" name="name" required>
					</div>
					<div class="form-group">
						<label>Hình ảnh</label>
					</div>
					<input accept="image/*" type="file" name="images" required/>
					<div class="form-group" style="text-align: right;">
						<input type="submit" class="btn btn-primary" name="add-row" value="Ok"/>
					</div>
				</form>
			</div>
		</div>

	</div>
</div>
