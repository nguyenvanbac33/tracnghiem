<%@ page import="java.util.ArrayList" %>
<%@ page import="com.nor.model.Course" %>
<%@ page import="com.nor.connection.Dao" %>
<%@ page import="com.nor.common.ParserUtils" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<jsp:include page="header.jsp"/>
<!-- Courses section -->
<section class="full-courses-section spad pt-0">
    <div class="container">
        <div class="row">
            <%
                boolean isMyCourse = request.getParameter("my-course") != null;
                String sqlCourse = "SELECT * FROM course";
                if (isMyCourse) {
                    String user = request.getSession().getAttribute("username").toString();
                    sqlCourse = "SELECT * FROM course a inner join register b on a.id = b.id_course where status = 1 and email = '" + user + "'";
                }
                ArrayList<Course> courses = Dao.getInstances().getData(Course.class, sqlCourse);
                for (Course c : courses) {
            %>
            <!-- course item -->
            <div class="col-lg-4 col-md-6 course-item">
                <a href="single-course.jsp?id=<%=c.getId()%>">
                    <div class="course-thumb">
                        <img height="250" src="<%= c.getImage()%>" alt="">
                        <div class="course-cat">
                            <span><%= c.getDuration()%></span>
                        </div>
                    </div>
                    <div class="course-info">
                        <h4><%= c.getName()%>
                        </h4>
                        <h4 class="cource-price">$<%= ParserUtils.parseMoney(c.getPrice())%>
                        </h4>
                    </div>
                </a>
            </div>
            <%
                }
            %>
        </div>
    </div>
</section>
<!-- Courses section end-->
<jsp:include page="footer.jsp"/>
