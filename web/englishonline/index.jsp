<%@ page import="com.nor.connection.Dao" %>
<%@ page import="com.nor.model.Post" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="com.nor.model.Course" %>
<%@ page import="com.nor.common.ParserUtils" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<jsp:include page="header.jsp"/>

<!-- Hero section -->
<section class="hero-section">
    <div class="hero-slider owl-carousel">
        <div class="hs-item set-bg" data-setbg="img/hero-slider/1.jpg">
            <div class="hs-text">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-8">
                            <div class="hs-subtitle">Award Winning UNIVERSITY</div>
                            <h2 class="hs-title">An investment in knowledge pays the best interest.</h2>
                            <p class="hs-des">Education is not just about going to school and getting a degree. It's
                                about widening your<br> knowledge and absorbing the truth about life. Knowledge is
                                power.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="hs-item set-bg" data-setbg="img/hero-slider/2.jpg">
            <div class="hs-text">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-8">
                            <div class="hs-subtitle">Award Winning UNIVERSITY</div>
                            <h2 class="hs-title">An investment in knowledge pays the best interest.</h2>
                            <p class="hs-des">Education is not just about going to school and getting a degree. It's
                                about widening your<br> knowledge and absorbing the truth about life. Knowledge is
                                power.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Hero section end -->

<!-- Services section -->
<section class="service-section spad">
    <div class="container services">
        <div class="section-title text-center">
            <h3>OUR SERVICES</h3>
            <p>We provides the opportunity to prepare for life</p>
        </div>
        <div class="row">
            <div class="col-lg-4 col-sm-6 service-item">
                <div class="service-icon">
                    <img src="img/services-icons/1.png" alt="1">
                </div>
                <div class="service-content">
                    <h4>Art Studio</h4>
                    <p>Lorem ipsum dolor sitdo amet, consectetur dont adipis elit. Vivamus interdum ultrices augue.
                        Aenean dos cursus lania.</p>
                </div>
            </div>
            <div class="col-lg-4 col-sm-6 service-item">
                <div class="service-icon">
                    <img src="img/services-icons/2.png" alt="1">
                </div>
                <div class="service-content">
                    <h4>Great Facility</h4>
                    <p>Lorem ipsum dolor sitdo amet, consectetur dont adipis elit. Vivamus interdum ultrices augue.
                        Aenean dos cursus lania.</p>
                </div>
            </div>
            <div class="col-lg-4 col-sm-6 service-item">
                <div class="service-icon">
                    <img src="img/services-icons/3.png" alt="1">
                </div>
                <div class="service-content">
                    <h4>Activity Hub</h4>
                    <p>Lorem ipsum dolor sitdo amet, consectetur dont adipis elit. Vivamus interdum ultrices augue.
                        Aenean dos cursus lania.</p>
                </div>
            </div>
            <div class="col-lg-4 col-sm-6 service-item">
                <div class="service-icon">
                    <img src="img/services-icons/4.png" alt="1">
                </div>
                <div class="service-content">
                    <h4>Fully Qualified</h4>
                    <p>Lorem ipsum dolor sitdo amet, consectetur dont adipis elit. Vivamus interdum ultrices augue.
                        Aenean dos cursus lania.</p>
                </div>
            </div>
            <div class="col-lg-4 col-sm-6 service-item">
                <div class="service-icon">
                    <img src="img/services-icons/5.png" alt="1">
                </div>
                <div class="service-content">
                    <h4>Flexible Schedule</h4>
                    <p>Lorem ipsum dolor sitdo amet, consectetur dont adipis elit. Vivamus interdum ultrices augue.
                        Aenean dos cursus lania.</p>
                </div>
            </div>
            <div class="col-lg-4 col-sm-6 service-item">
                <div class="service-icon">
                    <img src="img/services-icons/6.png" alt="1">
                </div>
                <div class="service-content">
                    <h4>Chemistry Lab</h4>
                    <p>Lorem ipsum dolor sitdo amet, consectetur dont adipis elit. Vivamus interdum ultrices augue.
                        Aenean dos cursus lania.</p>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Services section end -->

<!-- Event section -->
<section class="event-section spad">
    <div class="container">
        <div class="section-title text-center">
            <h3>New Course</h3>
        </div>
        <div class="row">
            <%
                String sqlCourse = "SELECT * FROM course order by id desc limit 4";
                ArrayList<Course> courses = Dao.getInstances().getData(Course.class, sqlCourse);
                for (Course c : courses) {
            %>
            <div class="col-md-6 course-item">
                <a href="single-course.jsp?id=<%=c.getId()%>">
                    <div class="course-thumb">
                        <img height="350" src="<%= c.getImage()%>" alt="">
                        <div class="course-cat">
                            <span><%= c.getDuration()%></span>
                        </div>
                    </div>
                    <div class="course-info">
                        <h4><%= c.getName()%>
                        </h4>
                        <h4 class="cource-price">$<%= ParserUtils.parseMoney(c.getPrice())%>
                        </h4>
                    </div>
                </a>
            </div>
            <%
                }
            %>
        </div>
    </div>
</section>
<!-- Event section end -->


<!-- Gallery section -->
<div class="gallery-section">
    <div class="gallery">
        <div class="grid-sizer"></div>
        <div class="gallery-item gi-big set-bg" data-setbg="img/gallery/1.jpg">
            <a class="img-popup" href="img/gallery/1.jpg"><i class="ti-plus"></i></a>
        </div>
        <div class="gallery-item set-bg" data-setbg="img/gallery/2.jpg">
            <a class="img-popup" href="img/gallery/2.jpg"><i class="ti-plus"></i></a>
        </div>
        <div class="gallery-item set-bg" data-setbg="img/gallery/3.jpg">
            <a class="img-popup" href="img/gallery/3.jpg"><i class="ti-plus"></i></a>
        </div>
        <div class="gallery-item gi-long set-bg" data-setbg="img/gallery/5.jpg">
            <a class="img-popup" href="img/gallery/5.jpg"><i class="ti-plus"></i></a>
        </div>
        <div class="gallery-item gi-big set-bg" data-setbg="img/gallery/8.jpg">
            <a class="img-popup" href="img/gallery/8.jpg"><i class="ti-plus"></i></a>
        </div>
        <div class="gallery-item gi-long set-bg" data-setbg="img/gallery/4.jpg">
            <a class="img-popup" href="img/gallery/4.jpg"><i class="ti-plus"></i></a>
        </div>
        <div class="gallery-item set-bg" data-setbg="img/gallery/6.jpg">
            <a class="img-popup" href="img/gallery/6.jpg"><i class="ti-plus"></i></a>
        </div>
        <div class="gallery-item set-bg" data-setbg="img/gallery/7.jpg">
            <a class="img-popup" href="img/gallery/7.jpg"><i class="ti-plus"></i></a>
        </div>
    </div>
</div>
<!-- Gallery section -->


<!-- Blog section -->
<section class="blog-section spad">
    <div class="container">
        <div class="section-title text-center">
            <h3>LATEST NEWS</h3>
            <p>Get latest breaking news & top stories today</p>
        </div>
        <div class="row">
            <%
                String sql = "SELECT * FROM `post` where id_course = 0";
                ArrayList<Post> items = Dao.getInstances().getData(Post.class, sql);
                for (Post item : items) {
            %>
            <div class="col-xl-6">
                <div class="blog-item">
                    <a href="single-blog.jsp?id=<%= item.getId()%>">
                        <div class="blog-thumb set-bg" data-setbg="<%= item.getImage()%>"></div>
                        <div class="blog-content">
                            <h4><%= item.getTitle()%>
                            </h4>
                            <div class="blog-meta">
                                <span><i class="fa fa-calendar-o"></i><%= item.getPubDate()%></span>
                            </div>
                            <p><%= item.getShortDesc()%>
                            </p>
                        </div>
                    </a>
                </div>
            </div>
            <%
                }
            %>
        </div>
    </div>
</section>
<!-- Blog section -->
<jsp:include page="footer.jsp"/>
