<%@ page import="com.nor.connection.Dao" %>
<%@ page import="com.nor.model.Test" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="com.nor.model.Question" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<jsp:include page="header.jsp"/>
<!-- Courses section -->
<section class="full-courses-section spad pt-0">
    <div class="container">
        <div class="row">
            <%
                String username = request.getSession().getAttribute("username").toString();
                String idCourse = request.getParameter("id");
                String sqlTest = "SELECT * FROM test where id_course = " + idCourse;
                ArrayList<Test> tests = Dao.getInstances().getData(Test.class, sqlTest);
                for (Test t : tests) {
                    String sqlQuestion = "SELECT a.*, b.answer as user_answer FROM question a join answer b on a.id = b.id_question and username = '" + username + "' where a.id_test = " + t.getId();
                    ArrayList<Question> questions = Dao.getInstances().getData(Question.class, sqlQuestion);
                    String status = questions.isEmpty() ? "Chưa làm" : "Đã làm";
            %>
            <!-- course item -->
            <div class="col-lg-4 col-md-6 course-item">
                <a href="single-test.jsp?id=<%=t.getId()%>">
                    <div class="course-thumb">
                        <img src="<%= t.getImage()%>" alt="">
                        <div class="course-cat">
                            <span><%= t.getPubDate()%></span>
                        </div>
                    </div>
                    <div class="course-info">
                        <h4><%= t.getName()%></h4>
                        <h6 style="color: silver"><%= status%></h6>
                    </div>
                </a>
            </div>
            <%
                }
            %>
        </div>
    </div>
</section>
<!-- Courses section end-->
<jsp:include page="footer.jsp"/>
