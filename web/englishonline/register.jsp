<jsp:include page="header.jsp"/>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
	<!-- Courses section -->
	<section class="contact-page spad pt-0">
		<div class="container">
			<div class="contact-form spad pb-0">
				<div class="section-title text-center">
					<h3>Register</h3>
				</div>
				<form class="comment-form --contact" action="/login" method="post">
					<div class="row">
						<div class="col-lg-4"></div>
						<div class="col-lg-4">
							<input type="text" name="name" placeholder="Your name">
						</div>
						<div class="col-lg-4"></div>
						<div class="col-lg-4"></div>
						<div class="col-lg-4">
							<input type="text" name="phone" placeholder="Phone">
						</div>
						<div class="col-lg-4"></div>
						<div class="col-lg-4"></div>
						<div class="col-lg-4">
							<input type="text" name="address" placeholder="Address">
						</div>
						<div class="col-lg-4"></div>
						<div class="col-lg-4"></div>
						<div class="col-lg-4">
							<input type="email" name="email" placeholder="Email">
						</div>
						<div class="col-lg-4"></div>
						<div class="col-lg-4"></div>
						<div class="col-lg-4">
							<input type="password" name="password" placeholder="Password">
						</div>
						<div class="col-lg-4"></div>
						<div class="col-lg-12">
							<div class="text-center">
								<button name="register" class="site-btn">Register</button>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</section>
	<!-- Courses section end-->
<jsp:include page="footer.jsp"/>
