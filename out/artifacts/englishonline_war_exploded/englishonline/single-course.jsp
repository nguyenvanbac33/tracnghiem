<%@ page import="com.nor.model.Post" %>
<%@ page import="com.nor.connection.Dao" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="com.nor.model.Course" %>
<%@ page import="com.nor.model.RegisterCourse" %>
<%@ page import="com.nor.model.Test" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<jsp:include page="header.jsp"/>
<%
    String email = request.getSession().getAttribute("username") + "";
    String id = request.getParameter("id");
    String sql = "SELECT * FROM `course` where id = " + id;
    Course item = Dao.getInstances().getData(Course.class, sql).get(0);
    sql = "SELECT * FROM register where id_course = " + id + " and email = '" + email + "'";
    ArrayList<RegisterCourse> registerCourses = Dao.getInstances().getData(RegisterCourse.class, sql);
    String title = "Hủy Đăng Ký";
    if (registerCourses.isEmpty() || registerCourses.get(0).getStatus().equals("-1")) {
        title = "Đăng Ký";
    }
%>
<!-- Blog page section -->
<section class="blog-page-section spad pt-0">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <div class="post-item post-details">
                    <img style="width: 100%" src="<%= item.getImage()%>" class="post-thumb-full" alt="">
                    <div class="post-content" id="news-content">
                        <h3><%= item.getName()%>
                        </h3>
                        <%
                            if (registerCourses.isEmpty() || registerCourses.get(0).getStatus().equals("-1") || registerCourses.get(0).getStatus().equals("0")) {
                        %>
                        <form action="/registerCourse" method="post">
                            <button value="<%= item.getId()%>" name="id" class="site-btn">
                                <%= title%>
                            </button>
                        </form>
                        <%
                            }
                        %>
                        <div class="post-meta" style="margin-top: 20px">
                            <span><i class="fa fa-calendar-o"></i> <%= item.getDuration()%></span>
                        </div>
                        <p><%= item.getSummary()%>
                        </p>
                        <p><%= item.getDesc()%>
                        </p>
                    </div>
                </div>
            </div>
            <!-- sidebar -->
            <div class="col-sm-8 col-md-5 col-lg-4 col-xl-3 offset-xl-1 offset-0 pl-xl-0 sidebar">
                <%
                    if (!registerCourses.isEmpty() && registerCourses.get(0).getStatus().equals("1")) {
                %>
                <!-- widget -->
                <div class="widget">
                    <h5 class="widget-title">Bài giảng</h5>
                    <div class="recent-post-widget">
                        <!-- recent post -->
                        <%
                            sql = "SELECT * FROM `post` where id_course = " + id + " ORDER BY id desc LIMIT 3";
                            ArrayList<Post> items = Dao.getInstances().getData(Post.class, sql);
                            for (Post i : items) {
                        %>
                        <div class="rp-item">
                            <a href="single-course.jsp?id=<%= i.getId()%>">
                                <div class="rp-thumb set-bg"  data-setbg="<%= i.getImage()%>"></div>
                                <div class="rp-content">
                                    <h6><%= i.getTitle()%></h6>
                                        <p><i class="fa fa-clock-o"></i> <%= i.getPubDate()%>
                                        </p>
                                </div>
                            </a>
                        </div>
                        <%
                            }
                        %>
                    </div>
                    <a class="site-btn" href="blog.jsp?id=<%= item.getId()%>">Xem Thêm</a>
                </div>
                <div class="widget">
                    <h5 class="widget-title">Bài kiểm tra</h5>
                    <div class="recent-post-widget">
                        <!-- recent post -->
                        <%
                            sql = "SELECT * FROM `test` where id_course = " + id + " ORDER BY id LIMIT 3";
                            ArrayList<Test> tests = Dao.getInstances().getData(Test.class, sql);
                            for (Test i : tests) {
                        %>
                        <div class="rp-item">
                            <a href="single-test.jsp?id=<%= i.getId()%>">
                                <div class="rp-thumb set-bg" data-setbg="<%= i.getImage()%>"></div>
                                <div class="rp-content">
                                    <h6><%= i.getName()%></h6>
                                        <p><i class="fa fa-clock-o"></i> <%= i.getPubDate()%>
                                        </p>
                                </div>
                            </a>
                        </div>
                        <%
                            }
                        %>
                    </div>
                    <a class="site-btn" href="test.jsp?id=<%= item.getId()%>">Xem Thêm</a>
                </div>
                <%
                } else {
                %>
                <div class="widget">
                    <h5 class="widget-title">Khóa học mới nhất</h5>
                    <div class="recent-post-widget">
                        <!-- recent post -->
                        <%
                            sql = "SELECT * FROM `course` ORDER BY id desc LIMIT 5";
                            ArrayList<Course> courses = Dao.getInstances().getData(Course.class, sql);
                            for (Course i : courses) {
                        %>
                        <div class="rp-item">
                            <a href="single-course.jsp?id=<%= i.getId()%>">
                                <div class="rp-thumb set-bg" data-setbg="<%= i.getImage()%>"></div>
                                <div class="rp-content">
                                    <h6><%= i.getName()%></h6>
                                        <p><i class="fa fa-clock-o"></i> <%= i.getDuration()%>
                                        </p>
                                </div>
                            </a>
                        </div>
                        <%
                            }
                        %>
                    </div>
                </div>
                <%
                    }
                %>
            </div>
        </div>
    </div>
</section>
<!-- Blog page section end -->
<style type="text/css">
    #news-content{
        text-align: justify;
        text-justify: inter-word;
    }
</style>
<jsp:include page="footer.jsp"/>
