<%@ page import="com.nor.model.Post" %>
<%@ page import="com.nor.connection.Dao" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="com.nor.model.Comment" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<jsp:include page="header.jsp"/>
<%
    String id = request.getParameter("id");
    String sql = "SELECT * FROM `post` where id = " + id;
    Post item = Dao.getInstances().getData(Post.class, sql).get(0);
%>
<!-- Blog page section -->
<section class="blog-page-section spad pt-0">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <div class="post-item post-details">
                    <img width="100%" src="<%= item.getImage()%>" class="post-thumb-full" alt="">
                    <div class="post-content" id="news-content">
                        <h3><%= item.getTitle()%>
                        </h3>
                        <div class="post-meta">
                            <span><i class="fa fa-calendar-o"></i><%= item.getPubDate()%></span>
                        </div>
                        <p><%= item.getShortDesc()%>
                        </p>
                        <p><%= item.getDesc()%>
                        </p>
                    </div>
                    <div class="comment-warp">
                        <%
                            sql = "SELECT * FROM comment where id_post = " + item.getId();
                            ArrayList<Comment> comments = Dao.getInstances().getData(Comment.class, sql);
                        %>
                        <h4 class="comment-title"><%= comments.size()%> Comments</h4>
                        <ul class="comment-list">
                            <%
                                for (Comment c : comments) {
                            %>
                            <li>
                                <div class="comment">
                                    <div class="comment-avator set-bg" style="background: lavender"><h3 style="margin-top: 25%; text-align: center"><%= c.getUsername().substring(0, 1)%></h3></div>
                                    <div class="comment-content">
                                        <span class="c-date"><%= c.getPubDate()%></span>
                                        <h5><%= c.getUsername()%>
                                        </h5>
                                        <p><%= c.getContent()%>
                                        </p>
                                    </div>
                                </div>
                            </li>
                            <%
                                }
                            %>
                        </ul>
                        <div class="comment-form-warp">
                            <h4 class="comment-title">Leave Your Comment</h4>
                            <form class="comment-form" method="post" action="/comment">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <textarea name="content" placeholder="Your Message"></textarea>
                                        <button class="site-btn" name="comment" value="<%= item.getId()%>">SEND
                                            COMMENT
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- sidebar -->
            <div class="col-sm-8 col-md-5 col-lg-4 col-xl-3 offset-xl-1 offset-0 pl-xl-0 sidebar">
                <!-- widget -->
                <div class="widget">
                    <h5 class="widget-title">Recent post</h5>
                    <div class="recent-post-widget">
                        <!-- recent post -->
                        <%
                            sql = "SELECT * FROM `post` where id <> " + id + " ORDER BY RAND() LIMIT 5";
                            ArrayList<Post> items = Dao.getInstances().getData(Post.class, sql);
                            for (Post i : items) {
                        %>
                        <div class="rp-item">
                            <a href="single-blog.jsp?id=<%= i.getId()%>">
                                <div class="rp-thumb set-bg" data-setbg="<%= i.getImage()%>"></div>
                                <div class="rp-content">
                                    <h6><%= i.getTitle()%>/h6>
                                        <p><i class="fa fa-clock-o"></i><%= i.getPubDate()%></p>
                                </div>
                            </a>
                        </div>
                        <%
                            }
                        %>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Blog page section end -->
<style type="text/css">
    #news-content{
        text-align: justify;
        text-justify: inter-word;
    }
</style>
<jsp:include page="footer.jsp"/>
