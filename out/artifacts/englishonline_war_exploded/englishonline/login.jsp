<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="header.jsp"/>

	<!-- Courses section -->
	<section class="contact-page spad pt-0">
		<div class="container">
			<div class="contact-form spad pb-0">
				<div class="section-title text-center">
					<h3>Login</h3>
				</div>
				<form class="comment-form --contact" method="post" action="/login">
					<div class="row">
						<div class="col-lg-4">
						</div>
						<div class="col-lg-4">
							<input type="text" name="username" placeholder="Your mail">
						</div>
						<div class="col-lg-4">
						</div>
						<div class="col-lg-4">
						</div>
						<div class="col-lg-4">
							<input type="password" name="password" placeholder="Your password">
						</div>
						<div class="col-lg-4">
						</div>
						<div class="col-lg-12">
							<div class="text-center">
								<button name="login" class="site-btn">Login</button>
								<br/>
								<a href="register.jsp" style="margin-top: 10px">Register</a>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</section>
	<!-- Courses section end-->
<jsp:include page="footer.jsp"/>
