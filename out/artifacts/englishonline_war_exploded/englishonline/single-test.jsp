<%@ page import="java.util.ArrayList" %>
<%@ page import="com.nor.model.Post" %>
<%@ page import="com.nor.connection.Dao" %>
<%@ page import="com.nor.model.Course" %>
<%@ page import="com.nor.model.Question" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<jsp:include page="header.jsp"/>
<!-- Blog page section -->
<section class="blog-page-section spad pt-0">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 post-list">
                <form method="post" action="/submitTest">
                    <%
                        int correct = 0;
                        String textSubmit = "Submit";
                        String disabled = "";
                        String username = request.getSession().getAttribute("username").toString();
                        String id = request.getParameter("id");
                        String sql = "SELECT a.*, b.answer as user_answer FROM question a left join answer b on a.id = b.id_question and username = '" + username + "' where a.id_test = " + id;
                        ArrayList<Question> questions = Dao.getInstances().getData(Question.class, sql);
                        int i = 0;
                        for (Question item : questions) {
                            i++;
                            String checkA = "a".equals(item.getUserAnswer()) ? "checked" : "";
                            String checkB = "b".equals(item.getUserAnswer()) ? "checked" : "";
                            String checkC = "c".equals(item.getUserAnswer()) ? "checked" : "";
                            String checkD = "d".equals(item.getUserAnswer()) ? "checked" : "";
                            String styleA = "";
                            String styleB = "";
                            String styleC = "";
                            String styleD = "";
                            if (item.getUserAnswer() != null && !item.getUserAnswer().isEmpty()) {
                                if (item.getUserAnswer().equalsIgnoreCase(item.getAnswer())) {
                                    correct++;
                                }
                                disabled = "disabled";
                                if (item.getAnswer().equalsIgnoreCase("a")) {
                                    styleA = "color: red";
                                } else if (item.getAnswer().equalsIgnoreCase("a")) {
                                    styleB = "color: red";
                                } else if (item.getAnswer().equalsIgnoreCase("a")) {
                                    styleC = "color: red";
                                } else {
                                    styleD = "color: red";
                                }
                                textSubmit = correct + "/" + questions.size();
                            }
                    %>
                    <div style="padding: 20px; box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2); transition: 0.3s; margin-top: 20px">
                        <h4>
                            <b>Question <%= i%>: </b><%= item.getQuestion()%>
                        </h4>
                        <%
                            if (item.getImage() != null && !item.getImage().isEmpty()) {
                        %>
                        <div>
                            <img src='<%= item.getImage()%>'>
                        </div>
                        <%
                            }
                        %>
                        <%
                            if (item.getAudio() != null && !item.getAudio().isEmpty()) {
                        %>
                        <audio controls style="margin: 20px">
                            <source src="<%= item.getAudio()%>" type="audio/mpeg">
                        </audio>
                        <%
                            }
                        %>
                        <fieldset id="<%= item.getId()%>" style="margin-top: 10px" required>
                            <p style="<%= styleA%>"><input type="radio" <%= disabled%> <%= checkA%> value="a"
                                                           name="<%= item.getId()%>"> <%= item.getA()%>
                            </p>
                            <p style="<%= styleB%>"><input type="radio" <%= disabled%> <%= checkB%> value="b"
                                                           name="<%= item.getId()%>"> <%= item.getB()%>
                            </p>
                            <p style="<%= styleC%>"><input type="radio" <%= disabled%> <%= checkC%> value="c"
                                                           name="<%= item.getId()%>"> <%= item.getC()%>
                            </p>
                            <p style="<%= styleD%>"><input type="radio" <%= disabled%> <%= checkD%> value="d"
                                                           name="<%= item.getId()%>"> <%= item.getD()%>
                            </p>
                        </fieldset>
                    </div>
                    <%
                        }
                    %>
                    <div class="text-center" style="margin-top: 20px">
                        <button <%= disabled%> name="submit" value="<%= id%>" onclick="return confirm('Are you sure?')"
                                class="site-btn"><%= textSubmit%>
                        </button>
                    </div>
                </form>
            </div>
            <!-- sidebar -->
            <div class="col-sm-8 col-md-5 col-lg-4 col-xl-3 offset-xl-1 offset-0 pl-xl-0 sidebar">
                <!-- widget -->
                <div class="widget">
                    <h5 class="widget-title">Khóa học mới nhất</h5>
                    <div class="recent-post-widget">
                        <!-- recent post -->
                        <%
                            sql = "SELECT * FROM course order by id desc limit 5";
                            ArrayList<Course> courses = Dao.getInstances().getData(Course.class, sql);
                            for (Course c : courses) {
                        %>
                        <div class="rp-item">
                            <a href="single-course.jsp?id=<%=c.getId()%>">
                                <div class="rp-thumb set-bg" data-setbg="<%= c.getImage()%>"></div>
                                <div class="rp-content">
                                    <h5><%= c.getName()%>
                                    </h5>
                                    <p><i class="fa fa-clock-o"></i> <%= c.getDuration()%>
                                    </p>
                                </div>
                            </a>
                        </div>
                        <%
                            }
                        %>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Blog page section end -->


<jsp:include page="footer.jsp"/>
