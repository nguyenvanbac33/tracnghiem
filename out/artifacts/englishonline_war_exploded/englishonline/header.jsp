<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<html lang="en">
<head>
    <title>Unica - Make english easy for everyone</title>
    <meta charset="UTF-8">
    <meta name="description" content="Unica University Template">
    <meta name="keywords" content="event, unica, creative, html">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Favicon -->
    <link href="img/favicon.ico" rel="shortcut icon"/>

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Rubik:400,400i,500,500i,700,700i" rel="stylesheet">

    <!-- Stylesheets -->
    <link rel="stylesheet" href="css/bootstrap.min.css"/>
    <link rel="stylesheet" href="css/font-awesome.min.css"/>
    <link rel="stylesheet" href="css/themify-icons.css"/>
    <link rel="stylesheet" href="css/magnific-popup.css"/>
    <link rel="stylesheet" href="css/animate.css"/>
    <link rel="stylesheet" href="css/owl.carousel.css"/>
    <link rel="stylesheet" href="css/style.css?version=1.0"/>


    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>
<body>
    <%
        if (request.getSession().getAttribute("result") != null) {
            int result = Integer.parseInt(request.getSession().getAttribute("result").toString());
            request.getSession().removeAttribute("result");
            if (result > 0) {
                out.println("<script type='text/javascript'>alert('Success');</script>");
            } else {
                out.println("<script type='text/javascript'>alert('Fail');</script>");
            }
        } else if (request.getSession().getAttribute("message") != null) {
            String msg = request.getSession().getAttribute("message").toString();
            request.getSession().removeAttribute("message");
            out.println("<script type='text/javascript'>alert('"+msg+"');</script>");
        }
    %>
<!-- Page Preloder -->
<div id="preloder">
    <div class="loader"></div>
</div>

<!-- header section -->
<header class="header-section">
    <div class="container">
        <!-- logo -->
        <a href="index.html" class="site-logo"><img src="img/logo.png" alt=""></a>
        <div class="nav-switch">
            <i class="fa fa-bars"></i>
        </div>
        <div class="header-info">
            <div class="hf-item">
                <i class="fa fa-clock-o"></i>
                <p><span>Working time:</span>Monday - Friday: 08 AM - 06 PM</p>
            </div>
            <div class="hf-item">
                <i class="fa fa-map-marker"></i>
                <p><span>Find us:</span>40 Xuan Thuy, Cau Giay, Ha Noi</p>
            </div>
        </div>
    </div>
</header>
<!-- header section end-->


<!-- Header section  -->
<nav class="nav-section">
    <div class="container">
        <div class="nav-right">
            <%
                if (request.getSession().getAttribute("username") == null) {
            %>
            <a href="login.jsp">Login</a>
            <%
            } else {
                String name = request.getSession().getAttribute("name").toString();
            %>
            <a href="course.jsp?my-course=1">My Course</a>
            <a href="/login?logout=1">Hi, <%= name%>
            </a>
            <%
                }
            %>
        </div>
        <ul class="main-menu">
            <li class="active"><a href="index.jsp">Home</a></li>
            <li><a href="course.jsp">Course</a></li>
            <li><a href="blog.jsp">Blog</a></li>
            <li><a href="about.jsp">About Us</a></li>
        </ul>
    </div>
</nav>
<!-- Header section end -->
<!-- Breadcrumb section -->
    <%
    String url[] = request.getRequestURL().toString().split("englishonline/");
    if (url.length > 1 && !url[1].startsWith("index")) {
        %>
<div class="site-breadcrumb">
    <div class="container">
        <a href="index.jsp"><i class="fa fa-home"></i> Home</a>
    </div>
</div>
    <%
    }
    %>
<!-- Breadcrumb section end -->
