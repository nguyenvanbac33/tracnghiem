<%@ page import="com.nor.model.Account" %>
<%@ page import="com.nor.connection.Dao" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<div id="insert" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Thêm quản trị</h4>
            </div>
            <div class="modal-body">
                <form method="post" action="/admin/user">
                    <div class="form-group">
                        <label>Tên đăng nhập</label> <input
                            type="text" class="form-control add-control" name="user_name" required>
                    </div>
                    <div class="form-group">
                        <label>Mật khẩu</label> <input
                            type="password" class="form-control add-control" name="password" required>
                    </div>
                    <div class="form-group">
                        <label>Họ tên</label> <input
                            type="text" class="form-control add-control" name="name" required>
                    </div>
                    <div class="form-group">
                        <label>Địa chỉ</label> <input
                            type="text" class="form-control add-control" name="address" required>
                    </div>
                    <div class="form-group">
                        <label>Số điện thoại</label> <input
                            type="text" class="form-control add-control" name="phone" required>
                    </div>
                    <div class="form-group" style="text-align: right;">
                        <input type="submit" class="btn btn-primary" name="add-row" value="Ok"/>
                    </div>
                </form>
            </div>
        </div>

    </div>
</div>
