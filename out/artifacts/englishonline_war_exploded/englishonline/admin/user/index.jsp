<%@ page import="java.util.ArrayList" %>
<%@ page import="com.nor.model.Account" %>
<%@ page import="com.nor.connection.Dao" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<div class="table-responsive">
    <table class="table table-hover">
        <thead class="text-primary">
        <th style="font-size: 15pt">
            Tên đăng nhập
        </th>
        <th style="font-size: 15pt">
            Họ tên
        </th>
        <th style="font-size: 15pt">
            Số điện thoại
        </th>
        <th style="font-size: 15pt">
            Địa chỉ
        </th>
        <th style="font-size: 15pt">
            Mật khẩu
        </th>
        <th width="50px" colspan="2"></th>
        </thead>
        <tbody>
        <%
            String key = "";
            if (request.getParameter("key") != null) {
                key = request.getParameter("key");
                key = new String(key.getBytes("ISO8859_1"), "UTF8");
            }
            String sql = "select * from account where type = 0 and (name like '%" + key + "%' or username like '%" + key + "%')";
            ArrayList<Account> admins = Dao.getInstances().getData(Account.class, sql);
            for (Account item : admins) {
        %>
        <tr>
            <td>
                <% out.println(item.getUserName()); %>
            </td>
            <td>
                <% out.println(item.getName()); %>
            </td>
            <td>
                <% out.println(item.getPhone()); %>
            </td>
            <td>
                <% out.println(item.getAddress()); %>
            </td>
            <td>
                <% out.println(item.getPassword()); %>
            </td>
            <td>
                <form method='post'>
                    <button type='submit' class='btn-control' class='btn-control' name='edit'
                            value='<% out.print(item.getUserName()); %>'><i
                            class='fa fa-edit'></i></button>
                </form>
            </td>
            <td>
                <form method='post' action="/admin/user">
                    <button type='submit' class='btn-control' class='btn-control' name='delete'
                            value='<% out.print(item.getUserName()); %>'><i
                            class='fa fa-trash'></i></button>
                </form>
            </td>
        </tr>
        <%
            }
        %>
        </tbody>
    </table>
</div>
<jsp:include page="add.jsp"/>
<jsp:include page="alter.jsp"/>
