<%@ page import="com.nor.connection.Dao" %>
<%@ page import="com.nor.model.Question" %>
<%@ page import="java.util.ArrayList" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%
    String id = request.getParameter("id");
    if (id == null || id.isEmpty()) {
        id = request.getSession().getAttribute("idTest").toString();
    } else {
        request.getSession().setAttribute("idTest", id);
    }
    String key = request.getParameter("key");
    key = key == null ? "" : new String(key.getBytes("ISO8859_1"), "UTF8");
    String sql = "SELECT * FROM question where id_test=" + id + " and question like '%" + key + "%'";
    ArrayList<Question> questions = Dao.getInstances().getData(Question.class, sql);
    for (Question item : questions) {
%>
<div>
    <h3 style="color: red">
        <%= item.getQuestion()%>
    </h3>
    <%
        if (item.getImage() != null && !item.getImage().isEmpty()) {
    %>
    <div>
        <img width=200 height=200 src='../<%= item.getImage()%>'>
    </div>
    <%
        }
    %>
    <%
        if (item.getAudio() != null && !item.getAudio().isEmpty()) {
    %>
    <audio controls style="margin: 20px">
        <source src="../<%= item.getAudio()%>" type="audio/mpeg">
    </audio>
    <%
        }
    %>
    <h5>A: <%= item.getA()%>
    </h5>
    <h5>B: <%= item.getB()%>
    </h5>
    <h5>C: <%= item.getC()%>
    </h5>
    <h5>D: <%= item.getD()%>
    </h5>
    <h5 style="color: red">Đáp án: <%= item.getAnswer()%>
    </h5>
    <form method="post">
        <div class="form-group">
            <button type="submit" class="btn btn-primary" name="edit" value="<%= item.getId()%>">
                Sửa
            </button>
        </div>
    </form>
    <form method="post" action="/admin/question">
        <div class="form-group">
            <button type='submit' class='btn btn-primary' name='delete' onclick="return confirm('Are you sure?')"
                    value='<%= item.getId()%>'>
                Xóa
            </button>
        </div>
    </form>
    <hr/>
</div>
<%
    }
%>
<jsp:include page="add.jsp"/>
<jsp:include page="alter.jsp"/>
