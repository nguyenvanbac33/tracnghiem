<%@ page import="com.nor.connection.Dao" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="com.nor.model.Post" %>
<%@ page import="com.nor.model.Course" %>
<%@ page import="com.nor.common.ParserUtils" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<div class="table-responsive">
    <table class="table">
        <thead class="text-primary">
        <th style="font-size: 15pt;">
            Hình ảnh
        </th>
        <th style="font-size: 15pt;">
            Tên khóa học
        </th>
        <th style="font-size: 15pt">
            Thời lượng
        </th>
        <th style="font-size: 15pt">
            Giá
        </th>
        <th style="font-size: 15pt">
            Bài giảng
        </th>
        <th style="font-size: 15pt">
            Bài test
        </th>
        <th width="25px" colspan="2"></th>
        </thead>
        <tbody>
        <%
            String key = request.getParameter("key");
            key = key == null ? "" : new String(key.getBytes("ISO8859_1"), "UTF8");
            String sql = "SELECT * FROM `course` where name like '%" + key + "%'";
            ArrayList<Course> items = Dao.getInstances().getData(Course.class, sql);
            for (Course item : items) {
        %>
        <tr>
            <td style=width:10%>
                <img width=100 height=100 src='../<%=item.getImage()%>'>
            </td>
            <td style=width:65%>
                <%= item.getName()%>
            </td>
            <td style=width:15%>
                <%= item.getDuration()%>
            </td>
            <td style=width:15%>
                <%= ParserUtils.parseMoney(item.getPrice())%>
            </td>
            <td style=width:15%>
                <a class='btn-control' href='?page=post&id=<% out.print(item.getId()); %>'><i class='fa fa-edit'></i></a>
            </td>
            <td style=width:15%>
                <a class='btn-control' href='?page=test&id=<% out.print(item.getId()); %>'><i class='fa fa-edit'></i></a>
            </td>
            <td style=width:5%>
                <form method='post'>
                    <button type='submit' class='btn-control' name='edit' value='<%= item.getId()%>'><i class='fa fa-edit'></i></button>
                </form>
            </td>
            <td style=width:5%>
                <form method='post' action="/admin/course">
                    <button type='submit' class='btn-control' name='delete' onclick="return confirm('Are you sure?')" value='<%= item.getId()%>'><i class='fa fa-trash'></i></button>
                </form>
            </td>
        </tr>
        <%
            }
        %>
        </tbody>
    </table>
</div>
<jsp:include page="add.jsp"/>
<jsp:include page="alter.jsp"/>
