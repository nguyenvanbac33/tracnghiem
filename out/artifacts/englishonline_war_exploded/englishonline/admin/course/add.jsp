<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<div id="insert" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Thêm khóa học</h4>
            </div>
            <div class="modal-body">
                <form id="fr-add-alphabet" method="post" enctype="multipart/form-data"
                      action="/admin/course">
                    <div class="form-group">
                        <label>Tên khóa học</label>
                        <br/>
                        <input type="text" class="form-control add-control" required name="name">
                    </div>
                    <div class="form-group">
                        <label>Thời gian</label>
                        <br/>
                        <input type="text" class="form-control add-control" required name="duration">
                    </div>
                    <div class="form-group">
                        <label>Giá</label>
                        <br/>
                        <input type="number" class="form-control add-control" required name="price">
                    </div>
                    <div class="form-group">
                        <label>Mô tả</label>
                        <br/>
                        <input type="text" class="form-control add-control" required name="summary">
                    </div>
                    <div class="form-group">
                        <label>Nội dung</label>
                        <br/>
                        <textarea name="desc" id="desc" required></textarea>
                        <script>CKEDITOR.replace('desc'); </script>
                    </div>
                    <label>Hình ảnh</label>
                    <br/>
                    <input accept="image/*" type="file" name="images"/>
                    <div class="form-group" style="text-align: right;">
                        <input type="submit" class="btn btn-primary" name="add-new-row" value="Ok"/>
                    </div>
                </form>
            </div>
        </div>

    </div>
</div>
