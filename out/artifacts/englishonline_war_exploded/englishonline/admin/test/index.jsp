<%@ page import="java.util.ArrayList" %>
<%@ page import="com.nor.model.Test" %>
<%@ page import="com.nor.connection.Dao" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<div class="table-responsive">
    <table class="table table-hover">
        <thead class="text-primary">
        <th style="font-size: 15pt;">
            Hình ảnh
        </th>
        <th style="font-size: 15pt">
            Tên đề thi
        </th>
        <th style="font-size: 15pt">
            Ngày
        </th>
        <th width="200px" style="font-size: 15pt">Câu hỏi</th>
        <th width="100px" colspan="2"></th>
        </thead>
        <tbody>
        <%
            String idCourse = request.getParameter("id");
            if (idCourse == null || idCourse.isEmpty()) {
                idCourse = request.getSession().getAttribute("idCourse").toString();
            } else {
                request.getSession().setAttribute("idCourse", idCourse);
            }
            String key = request.getParameter("key");
            key = key == null ? "" : new String(key.getBytes("ISO8859_1"), "UTF8");
            String sql = "SELECT * FROM `test` where id_course = "+idCourse+" and name like  '%" + key + "%'";
            ArrayList<Test> groups = Dao.getInstances().getData(Test.class, sql);
            for (Test item : groups) {
        %>
        <tr>
            <td style=width:10%>
                <img width=100 height=100 src='../<%=item.getImage()%>'>
            </td>
            <td>
                <% out.println(item.getName()); %>
            </td>
            <td>
                <%= item.getPubDate()%>
            </td>
            <td>
                <a class='btn-control' href='?page=question&id=<% out.print(item.getId()); %>'><i class='fa fa-edit'></i></a>
            <td>
                <form method='post'>
                    <button type='submit' class='btn-control' class='btn-control' name='edit'
                            value='<% out.print(item.getId()); %>'><i
                            class='fa fa-edit'></i>
                    </button>
                </form>
            </td>
            <td>
                <form method='post' action="/admin/test">
                    <button type='submit' class='btn-control' onclick="return confirm('Are you sure?')" class='btn-control' name='delete'
                            value='<% out.print(item.getId()); %>'><i
                            class='fa fa-trash'></i></button>
                </form>
            </td>
        </tr>
        <%
            }
        %>
        </tbody>
    </table>
</div>
<jsp:include page="alter.jsp"/>
<jsp:include page="add.jsp"/>
