<%@ page import="com.nor.model.Test" %>
<%@ page import="com.nor.connection.Dao" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<div id="update-row" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Cập nhập đề thi</h4>
			</div>
			<div class="modal-body">
				<form method="post" action="/admin/test" enctype="multipart/form-data">
					<%
						if(request.getParameter("edit") != null){
							String id =  request.getParameter("edit");
							String sql = "select * from `test` where id = '"+id+"'";
							Test item = Dao.getInstances().getData(Test.class, sql).get(0);
					%>
					<div class="form-group">
						<label>Mã đề thi</label> <input
							type="text" class="form-control add-control" value="<%= item.getId() %>" readonly="readonly" name="edit" required>
					</div>
					<div class="form-group">
						<label>Tên đề thi</label> <input
							type="text" class="form-control add-control" value="<%= item.getName() %>" name="name" required>
					</div>
					<div class="form-group">
						<label>Hình ảnh</label>
					</div>
					<input accept="image/*" type="file" name="images"/>
					<div class="form-group" style="text-align: right;">
						<input type="submit" class="btn btn-primary" name="submit-edit-row" value="Ok"/>
					</div>
					<script type="text/javascript">
						$('#update-row').modal('show');
					</script>
					<%
						}
					%>
				</form>
			</div>
		</div>

	</div>
</div>
