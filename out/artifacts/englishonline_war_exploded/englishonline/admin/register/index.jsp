<%@ page import="com.nor.connection.Dao" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="com.nor.model.RegisterCourse" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%
    String sql = "SELECT a.*, b.*, c.name as course FROM register a inner join account b on a.email = b.username inner join course c on a.id_course = c.id order by id desc";
    ArrayList<RegisterCourse> carts = Dao.getInstances().getData(RegisterCourse.class, sql);
    for (RegisterCourse item : carts) {
%>
<div>
    <h3 style="color: red">
        <b>Khóa học: <%= item.getCourse()%>
        </b>
    </h3>
    <h5>
        <b>Người đăng ký: <%= item.getName()%>
        </b>
    </h5>
    <h5>Địa chỉ: <%= item.getAddress()%>
    </h5>
    <h5>Số điện thoại: <%= item.getPhone()%>
    </h5>
    <h5>Email: <%= item.getMail()%>
    </h5>
    <h5>Ngày đăng ký: <%= item.getPubDate()%>
    </h5>
    <h5 style="color: red"><%= item.getStatusDisplay()%>
    </h5>
    <%
        if (Integer.parseInt(item.getStatus()) < 1) {
    %>
    <form method="post" action="/confirmCourse">
        <div class="form-group" style="text-align: right;">
            <button type="submit" class="btn btn-primary" name="confirm" value="<%= item.getId()%>">Xác nhận</button>
            <button style="margin-left: 10px;" type="submit" value="<%= item.getId()%>" name="cancel" id='add'
                    class='btn btn-primary'>Huỷ
            </button>
        </div>
    </form>
    <%
        }
    %>
    <hr/>
</div>
<%
    }
%>
