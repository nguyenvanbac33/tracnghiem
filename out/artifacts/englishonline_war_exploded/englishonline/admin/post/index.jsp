<%@ page import="com.nor.connection.Dao" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="com.nor.model.Post" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<div class="table-responsive">
    <table class="table">
        <thead class="text-primary">
        <th style="font-size: 15pt;">
            Hình ảnh
        </th>
        <th style="font-size: 15pt;">
            Tiêu đề
        </th>
        <th style="font-size: 15pt">
            Ngày
        </th>
        <th width="25px" colspan="2"></th>
        </thead>
        <tbody>
        <%
            String idCourse = request.getParameter("id");
            if (idCourse == null || idCourse.isEmpty()) {
                idCourse = request.getSession().getAttribute("idCourse").toString();
            } else {
                request.getSession().setAttribute("idCourse", idCourse);
            }
            String key = request.getParameter("key");
            key = key == null ? "" : new String(key.getBytes("ISO8859_1"), "UTF8");
            String sql = "SELECT * FROM `post` where id_course = "+idCourse+" and title like '%" + key + "%'";
            ArrayList<Post> items = Dao.getInstances().getData(Post.class, sql);
            for (Post item : items) {
        %>
        <tr>
            <td style=width:10%>
                <img width=100 height=100 src='../<%=item.getImage()%>'>
            </td>
            <td style=width:65%>
                <%= item.getTitle()%>
            </td>
            <td style=width:15%>
                <%= item.getPubDate()%>
            </td>
            <td style=width:5%>
                <form method='post'>

                    <button type='submit' class='btn-control' name='edit' value='<%= item.getId()%>'><i class='fa fa-edit'></i></button>
                </form>
            </td>
            <td style=width:5%>
                <form method='post' action="/admin/post">
                    <button type='submit' class='btn-control' name='delete' onclick="return confirm('Are you sure?')" value='<%= item.getId()%>'><i class='fa fa-trash'></i></button>
                </form>
            </td>
        </tr>
        <%
            }
        %>
        </tbody>
    </table>
</div>
<jsp:include page="add.jsp"/>
<jsp:include page="alter.jsp"/>
