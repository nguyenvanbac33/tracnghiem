<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<div id="insert" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Thêm bài viết</h4>
            </div>
            <div class="modal-body">
                <form id="fr-add-alphabet" method="post" enctype="multipart/form-data" action="/admin/post">
                    <div class="form-group">
                        <label>Tiêu đề</label> <input
                            type="text" maxlength="100" class="form-control add-control" name="title" required>
                    </div>
                    <div class="form-group">
                        <label>Mô tả</label>
                        <br/>
                        <input type="text" maxlength="200" class="form-control add-control" required name="short_desc">
                    </div>
                    <div class="form-group">
                        <label>Chi tiết</label>
                        <br/>
                        <textarea name="desc" id="desc" required></textarea>
                        <script>CKEDITOR.replace('desc'); </script>
                    </div>
                    <div class="form-group">
                        <label>Hình ảnh</label>
                    </div>
                    <input accept="image/*" type="file" name="images" required/>
                    <div class="form-group" style="text-align: right;">
                        <input type="submit" class="btn btn-primary" name="add-new-row" value="Ok"/>
                    </div>
                </form>
            </div>
        </div>

    </div>
</div>
