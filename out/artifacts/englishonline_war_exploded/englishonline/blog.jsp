<%@ page import="java.util.ArrayList" %>
<%@ page import="com.nor.model.Post" %>
<%@ page import="com.nor.connection.Dao" %>
<%@ page import="com.nor.model.Course" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<jsp:include page="header.jsp"/>
<!-- Blog page section -->
<section class="blog-page-section spad pt-0">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 post-list">
                <%
                    String id = request.getParameter("id");
                    id = id == null ? "0" : id;
                    String sql = "SELECT * FROM post where id_course = " + id;
                    ArrayList<Post> posts = Dao.getInstances().getData(Post.class, sql);
                    for (Post p : posts) {
                %>
                <div class="post-item">
                    <div class="post-thumb set-bg" data-setbg="<%= p.getImage()%>"></div>
                    <div class="post-content">
                        <h3><a href="single-blog.jsp?id=<%=p.getId()%>"><%= p.getTitle()%>
                        </a></h3>
                        <div class="post-meta">
                            <span><i class="fa fa-calendar-o"></i> <%= p.getPubDate()%></span>
                        </div>
                        <p><%= p.getShortDesc()%>
                        </p>
                    </div>
                </div>
                <%
                    }
                %>
            </div>
            <!-- sidebar -->
            <div class="col-sm-8 col-md-5 col-lg-4 col-xl-3 offset-xl-1 offset-0 pl-xl-0 sidebar">
                <!-- widget -->
                <div class="widget">
                    <h5 class="widget-title">Khóa học mới nhất</h5>
                    <div class="recent-post-widget">
                        <!-- recent post -->
                        <%
                            sql = "SELECT * FROM course order by id desc limit 5";
                            ArrayList<Course> courses = Dao.getInstances().getData(Course.class, sql);
                            for (Course c : courses) {
                        %>
                        <div class="rp-item">
                            <a href="single-course.jsp?id=<%=c.getId()%>">
                                <div class="rp-thumb set-bg" data-setbg="<%= c.getImage()%>"></div>
                                <div class="rp-content">
                                    <h5><%= c.getName()%>
                                    </h5>
                                    <p><i class="fa fa-clock-o"></i> <%= c.getDuration()%>
                                    </p>
                                </div>
                            </a>
                        </div>
                        <%
                            }
                        %>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Blog page section end -->


<jsp:include page="footer.jsp"/>
